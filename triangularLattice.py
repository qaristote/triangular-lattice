from math import *
from typing import Tuple, List

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri

import pickle

DOWN = False
UP = True


class Triangle() :
    """ A triangle in the lattice.

    :param vertices: The tuple of the three vertices of the triangle. 
                     Vertex i is between edges i-1 and i.
    :param spins: The tuple of spins (↑ or ↓) associated with each edge.
    :param neighbors: The triangles that lie opposite of each edge of the current triangle.
    :param psi: The value of ψ on each (interior) edge of the triangle."""
    
    def __init__(self, vertices : Tuple[complex, complex, complex],
                 spins : Tuple[bool, bool, bool], 
                 neighbors : List[Tuple[complex, complex, complex]] = [None] * 3,
                 psi : np.ndarray = np.zeros(3, dtype = complex)) :
        self.vertices = vertices
        self.center = np.mean(self.vertices)
        self.distance = np.absolute(self.center)
        self.neighbors = neighbors.copy()
        self.psi = psi.copy()
        self.spins = spins
        self.edges = ((vertices[0], vertices[1]),
                      (vertices[1], vertices[2]),
                      (vertices[2], vertices[0]))

        
    def rotate(self) :
        """Rotate the values of ψ around the triangle."""
        
        self.psi = np.array([self.psi[i - 1] for i in range(3)])
        
            
    def probability(self) :
        """Return the probability for the particle to be inside the triangle.

        :rtype: float"""
        
        return np.sum(np.absolute(self.psi) ** 2)


    def is_basic(self) :
        """Check whether the triangle is part of the initial lattice or not.

        :rtype: bool"""

        return ((self.vertices[1] - self.vertices[0]) * (self.vertices[2] - self.vertices[0]) == 1j)

    
    def pprint(self) :
        """Print the triangle's attributes."""
        
        print('{} :'.format(self))
        print('\tVertices : {}'.format(self.vertices))
        print('\tNeighbors :\n\t\t0 : {}\n\t\t1 : {}\n\t\t2 : {}'.format(self.neighbors[0],
                                                                         self.neighbors[1],
                                                                         self.neighbors[2]))
        print('\tψ: {}'.format(self.psi))
        print('\tSpins :\n\t\t0 : {}\n\t\t1 : {}\n\t\t2 : {}'.format('↑' if self.spins[0] else '↓',
                                                                     '↑' if self.spins[1] else '↓',
                                                                     '↑' if self.spins[2] else '↓'))
        
    

        
class Edge() :
    """An edge in the lattice, between two triangles.

    :param coords: The vertices of the edge.
    :param port: The port associated with the edge.
    :param down: The triangle lying along the edge whose spin is ↓.
    :param up: The triangle lying along the edge whose spin is ↑."""
    
    def __init__(self, coords : Tuple[complex, complex],
                 port : int,
                 down : Tuple[complex, complex, complex] = None,
                 up : Tuple[complex, complex, complex] = None) :
        self.coords = coords
        self.port = port % 3
        self.triangles = [down, up]
        self.middle = np.mean(coords)


    
    def pprint(self) :
        """Print the edge's attribute."""
        
        print(self)
        print('\tCoordinates : {}'.format(self.coords))
        print('\tPort : {}'.format(self.port))
        print('\tTriangles :\n\t\t↑ : {}\n\t\t↓ : {}'.format(self.triangles[UP],
                                                             self.triangles[DOWN]))



        
class Lattice():
    """A lattice with a single triangle.

    :param psi: An array of complex numbers for the value of ψ on the initial triangle.
                Its norm should be equal to 1."""
    
    def __init__(self, psi : np.ndarray = (1/sqrt(3) + 0j) * np.ones(3)) :
        origin = Triangle((0,1,1j),
                          (DOWN, DOWN, DOWN),
                          # neighbors = [None, None, None]
                          psi = psi.copy())

        self.triangles = {origin.vertices : origin}
        # If (P1,P2) is in edges, (P2,P1) must not be
        self.edges = {origin.edges[i] : Edge(origin.edges[i], i, down = origin.vertices) for i in range(3)}
        self.radius = origin.distance
        # The list of three cycles ordered by increasing probability
        self.three_cycles = set()
        # The number of triangles around each vertex
        # Used to compute the Ricci curvature
        self.surrounding_triangles = {0 : 6, 1 : 6, 1j : 6}


    # --- Computations ---
    
    def cartesian(self, points : np.ndarray) :
        """Get the cartesian coordinates of a vector given in the triangular basis.

        :param point: The point whose coordinates to return."""
        
        return np.array([[1, .5], [0, sqrt(3) / 2]]) @ [np.real(points), np.imag(points)]


    def is_in_ball(self, center : complex, radius : float, point : complex) :
        """Whether a point (in triangular coordinates) is a a ball.

        :param center: The center of the ball.
        :param radius: The radius of the ball.
        :param point: The point we're inquiring about."""

        coords = self.cartesian(point)
        return np.absolute(coords[0] + 1j * coords[1] - center) <= radius

    
    def edge_probability(self, edge : Edge) :
        """Return the probability for the particle to be on this edge.
        
        :param edge: The edge whose probability to return.
        :rtype: float"""
        
        return np.sum(np.absolute(self.triangles[edge.triangles[i]].psi[edge.port]) ** 2
                      if edge.triangles[i] in self.triangles
                      else 0
                      for i in (DOWN, UP))

    
    def three_cycle_probability(self, vertices : Tuple[Tuple[complex, complex, complex],
                                                       Tuple[complex, complex, complex],
                                                       Tuple[complex, complex, complex]]) :
        """Return the probability for the particle to be inside a 3-cycle.

        :param triangles: A tuple of triangle that make a cycle.
                          The structure of the cycle has to be the following :
                  triangles[2]
                    /     \
                   /       \
                  /         \
                 1           0
                /             \
               /               \
              /                 \
         triangles[0]---2---triangles[1]
        :rtype: float"""

        return np.sum(np.absolute(self.triangles[vertices[i]].psi[i - 1]) ** 2 +
                      np.absolute(self.triangles[vertices[i]].psi[i - 2]) ** 2
                      for i in range(3))

    def variance(self) :
        """Compute the x and y-variance of the particle's position in the lattice."""

        weights, positions = np.array([[self.edge_probability(e), e.middle] for e in self.edges.values()]).T
        positions = self.cartesian(positions).T

        avg = np.average(positions, axis = 0, weights = weights)
        return np.real(np.average((positions - avg) ** 2, axis = 0, weights = weights))


    def curvature(self, vertex : complex) :
        """Compute the Ricci curvature of a vertex.

        :param vertex: The vertex at which to compute the curvature."""

        if vertex in self.surrounding_triangles :
            return pi * (2 - self.surrounding_triangles[vertex] / 3)
        else :
            return 0
        

    # --- Elementary graph transformations ---

    def apply_unitary(self, unitaries : Tuple[np.matrixlib.defmatrix.matrix,
                                              np.matrixlib.defmatrix.matrix,
                                              np.matrixlib.defmatrix.matrix]) :
        """Apply unitaries to edges depending on their ports.

        :param unitaries: The unitaries to apply for each port."""
        
        for e in list(self.edges.values()) :
            if self.edge_probability(e) > 0 :
                if e.triangles[UP] is None or e.triangles[DOWN] is None :
                    self.add_triangle_along(e.coords)
                up = self.triangles[e.triangles[UP]]
                down = self.triangles[e.triangles[DOWN]]
                [up.psi[e.port], down.psi[e.port]] = np.array(unitaries[e.port] @ (up.psi[e.port], down.psi[e.port]))[0]

    
    def add_triangle_along(self, coords : Tuple[complex, complex]) :
        """Add a triangle along an edge specified by its coordinates.

        :param coords: The edge's vertices' coordinates."""

        # print('Adding triangle along {} ...'.format(coords))
        # Check for consistency and find the edge from its coordinates
        if coords in self.edges :
            edge = self.edges[coords]
        elif coords[::-1] in self.edges :
            edge = self.edges[coords[::-1]]
        else :
            raise ValueError('edge not found')
        if edge.triangles[DOWN] is None and edge.triangles[UP] is None :
            raise ValueError('edge doesn\'t touch any triangle')
        elif not(edge.triangles[DOWN] is None or edge.triangles[UP] is None) :
            edge.pprint()
            edge.triangles[UP].pprint()
            edge.triangles[DOWN].pprint()
            raise ValueError('edge is already between two triangles')
        else :
            
            # Build the new triangle
            spin = edge.triangles.index(None)
            neighbor = self.triangles[edge.triangles[not(spin)]]
            vertex0 = neighbor.vertices[edge.port - 2]
            vertex1 = neighbor.vertices[edge.port]
            vertex2 = neighbor.vertices[edge.port - 1]
            if (np.conj(vertex0 - vertex1) * (vertex2 - vertex1)).imag >= 0 :
                new_vertex = vertex1 + np.array([-1 + 1j, -1]) @ ((vertex1 - vertex0).real, (vertex1 - vertex0).imag)
                vertices = [vertex0, vertex1, new_vertex]
            else :
                new_vertex = vertex0 + np.array([-1 + 1j, -1]) @ ((vertex0 - vertex1).real, (vertex0 - vertex1).imag)
                vertices = [vertex1, vertex0, new_vertex]
            triangle = Triangle(tuple(vertices[-edge.port:] + vertices[:-edge.port]),
                                (spin, spin, spin))
            
            # Complete the lattice
            triangle.neighbors[edge.port] = neighbor.vertices
            neighbor.neighbors[edge.port] = triangle.vertices
            edge.triangles[spin] = triangle.vertices
            self.triangles[triangle.vertices] = triangle
            for i in (edge.port - 1, edge.port - 2) :
                coords = None
                if triangle.edges[i] in self.edges :
                    coords = triangle.edges[i]
                elif triangle.edges[i][::-1] in self.edges :
                    coords = triangle.edges[i][::-1]
                if coords is None :
                    self.edges[triangle.edges[i]] = Edge(triangle.edges[i],
                                                         i,
                                                         up = triangle.vertices if spin else None,
                                                         down = None if spin else triangle.vertices)
                else :
                    edge = self.edges[coords]
                    if not(edge.triangles[spin] is None) :
                        raise ValueError('another triangle already exists at the same spot')
                    edge.triangles[spin] = triangle.vertices
                    if not(edge.triangles[not(spin)] is None) :
                        neighbor = self.triangles[edge.triangles[not(spin)]]
                        triangle.neighbors[i] = neighbor.vertices
                        neighbor.neighbors[i] = triangle.vertices
            self.radius = max(self.radius, triangle.distance)
            if not(new_vertex in self.surrounding_triangles) :
                self.surrounding_triangles[new_vertex] = 6
            return triangle


    def remove_triangle(self, triangle : Triangle) :
        """Remove a triangle from the lattice.

        :param triangle: The triangle to remove."""

        for i in range(3) :
            if triangle.edges[i] in self.edges :
                coords = triangle.edges[i]
            elif triangle.edges[i][::-1] in self.edges :
                coords = triangle.edges[i][::-1]
            else :
                raise ValueError('edge not found')
            if triangle.neighbors[i] is None :
                del self.edges[coords]
            else :
                self.triangles[triangle.neighbors[i]].neighbors[i] = None
                self.edges[coords].triangles[triangle.spins[i]] = None
        del self.triangles[triangle.vertices]
        for i in range(3) :
            if all(triangle.vertices[i] not in e for e in self.edges) :
                del self.surrounding_triangles[triangle.vertices[i]]

                
    def translate(self, new_value : complex,
                  triangle : Triangle,
                  ports : Tuple[int, int]) :
        """_Translate along a sequence of two ports. Used in 3-to-1 Pachner moves.

        :param new_value: The value with which to replace the current value of ψ."""

        # Check for consistency
        if not(triangle.vertices in self.triangles) :
            raise ValueError('triangle not found')
        elif ports[0] == ports[1] :
            raise ValueError('same ports')
        else :
            while True : # we exit the function with the returns
                # print('Translating {} along {} ...'.format(triangle.vertices, ports))
                # Write the new value of psi
                old_value = triangle.psi[ports[0]]
                triangle.psi[ports[0]] = new_value
                
                # Find the next translation to do
                next_triangle_vertices = triangle.neighbors[ports[1]]
                if next_triangle_vertices is None :
                    if triangle.distance >= self.radius and old_value == 0 :
                        return
                    else :
                        next_triangle_vertices = self.add_triangle_along(triangle.edges[ports[1]]).vertices
                next_triangle = self.triangles[next_triangle_vertices]
                triangle_vertices = next_triangle.neighbors[ports[0]]
                if triangle_vertices is None :
                    if next_triangle.distance >= self.radius and old_value == 0 :
                        return
                    else :
                        triangle_vertices = self.add_triangle_along(next_triangle.edges[ports[0]]).vertices
                triangle = self.triangles[triangle_vertices]
                new_value = old_value


    # --- Complex transformations ---
    
    def one_to_three(self, triangle : Triangle) :
        """Make a 1-to-3 Pachner move.

        :param triangle: The triangle onto which the move is applied."""

        # print('1-to-3 Pachner move on {} ...'.format(triangle.vertices))
        # Check for consistency
        if not(triangle.vertices in self.triangles) :
            raise ValueError('triangle not found')
        else :
            
            # Build the three new triangles
            triangles = [None, None, None]
            triangles[0] = Triangle((triangle.vertices[1], triangle.vertices[0], triangle.center),
                                    triangle.spins,
                                    psi = np.array((triangle.psi[0], 0j, 0j)))
            triangles[1] = Triangle((triangle.center, triangle.vertices[2], triangle.vertices[1]),
                                    (triangle.spins[0], triangle.spins[1], not(triangle.spins[2])),
                                    psi = np.array((0j, triangle.psi[1], 0j)))
            triangles[2] = Triangle((triangle.vertices[2], triangle.center, triangle.vertices[0]),
                                    (not(triangle.spins[0]), not(triangle.spins[1]), triangle.spins[2]),
                                    psi = np.array((0j, 0j, triangle.psi[2])))
            triangles[0].neighbors = [triangle.neighbors[0], triangles[2].vertices, triangles[1].vertices]
            triangles[1].neighbors = [triangles[2].vertices, triangle.neighbors[1], triangles[0].vertices]
            triangles[2].neighbors = [triangles[1].vertices, triangles[0].vertices, triangle.neighbors[2]]
            
            # Update the sets of triangles and edges
            del self.triangles[triangle.vertices]
            for i in range(3) :
                
                # Old edges
                if triangle.edges[i] in self.edges :
                    coords = triangle.edges[i]
                else :
                    coords = triangle.edges[i][::-1]
                self.edges[coords].triangles[triangle.spins[i]] = triangles[i].vertices
                self.triangles[triangles[i].vertices] = triangles[i]
                if not(triangle.neighbors[i] is None) :
                    self.triangles[triangle.neighbors[i]].neighbors[i] = triangles[i].vertices

                # New edges
                coords = triangles[i].edges[i - 2]
                spin = triangles[i].spins[i - 2]
                edge = Edge(coords, i - 2)
                self.edges[coords] = edge
                edge.triangles[spin] = triangles[i].vertices
                edge.triangles[not(spin)] = triangles[i].neighbors[i - 2]

            # Update the list of 3-cycles
            self.three_cycles.add((triangles[0].vertices, triangles[1].vertices, triangles[2].vertices))
            # Update the number of surrounding triangles
            for v in triangle.vertices :
                self.surrounding_triangles[v] += 1
            self.surrounding_triangles[triangle.center] = 3

            
    def three_to_one(self, vertices : Tuple[Tuple[complex, complex, complex],
                                            Tuple[complex, complex, complex],
                                            Tuple[complex, complex, complex]]) :
        """Make a 3-to-1 Pachner move.

        :param triangles: A tuple of triangles that make a cycle, onto which the move is applied.
                          The structure of the cycle has to be the following :
                  triangles[2]
                    /     \
                   /       \
                  /         \
                 1           0
                /             \
               /               \
              /                 \
         triangles[0]---2---triangles[1]"""

        """print('3-to-1 Pachner move on {}, {} and {} ...'.format(vertices[0],
                                                                vertices[1],
                                                                vertices[2]))"""
        
        # Check for consistency
        if not(vertices[0] in self.triangles
               or vertices[1] in self.triangles
               or vertices[2] in self.triangles) :
            raise ValueError('one of the triangles is not part of the lattice')
        triangles = tuple(self.triangles[vertices[i]] for i in range(3))
        if (triangles[0].neighbors[2] != triangles[1].vertices
            or triangles[1].neighbors[0] != triangles[2].vertices
            or triangles[2].neighbors[1] != triangles[0].vertices) :
            raise ValueError('the triangles do not make a 3-cycle')
        elif (triangles[1].neighbors[2] != triangles[0].vertices
              or triangles[2].neighbors[0] != triangles[1].vertices
              or triangles[0].neighbors[1] != triangles[2].vertices) :
            raise ValueError('the edges are not symmetric')
        else :
            
            # Change the walker's state
            self.translate(0j, triangles[0], (1,0))
            self.translate(0j, triangles[0], (2,0))
            self.translate(0j, triangles[1], (0,1))
            self.translate(0j, triangles[1], (2,1))
            self.translate(0j, triangles[2], (0,2))
            self.translate(0j, triangles[2], (1,2))

            # Build the new triangle
            triangle = Triangle((vertices[0][1], vertices[1][2], vertices[2][0]),
                                (triangles[0].spins[0], triangles[1].spins[1], triangles[2].spins[2]),
                                [triangles[0].neighbors[0], triangles[1].neighbors[1], triangles[2].neighbors[2]],
                                np.array([triangles[0].psi[0], triangles[1].psi[1], triangles[2].psi[2]]))
            
            # Update the lattice
            ## Add the new triangle
            self.triangles[triangle.vertices] = triangle
            
            for i in range(3) :
                ## Remove the old triangles and edges
                del self.triangles[vertices[i]]
                coords = (triangles[i].edges[i - 2] if triangles[i].edges[i - 2] in self.edges
                          else triangle[i].edges[i - 2][::-1])
                del self.edges[coords]

                ## Update the edges
                if not(triangle.neighbors[i] is None) :
                    self.triangles[triangle.neighbors[i]].neighbors[i] = triangle.vertices
                coords = (triangle.edges[i] if triangle.edges[i] in self.edges
                          else triangle.edges[i][::-1])
                edge = self.edges[coords]
                edge.triangles[triangle.spins[i]] = triangle.vertices

            ## Remove the old cycle
            self.three_cycles.remove(vertices)
            
            ## Update the number of surrounding triangles
            del self.surrounding_triangles[triangles[0].vertices[2]]
            for i in range(3) :
                self.surrounding_triangles[vertices[i][i]] -= 1

                
    # --- User-level functions

    def step(self, coin : np.matrixlib.defmatrix.matrix,
             codings : Tuple[np.matrixlib.defmatrix.matrix,
                             np.matrixlib.defmatrix.matrix,
                             np.matrixlib.defmatrix.matrix],
             alpha : float,
             beta : float,
             make_pachner_moves : bool = True) :
        """Go from the state at time t to the state at time t + ε.

        :param coin: The coin (W) to use on \tilde{Psi}, that is after the edges were encoded.
        :param codings: The codings (U_0, U_1, U_2) applied on the edges to get \tilde{Psi} from Psi.
        :param alpha: The minimal probability for a triangle to have to make a 1-to-3 move onto it.
        :param beta: The maximal probability for a 3-cycle to have to make a 3-to-1 move onto it.
        :param make_pachner_moves: A boolean to choose whether or not to make Pachner moves."""

        # print("Making a step ...")

        decodings = tuple(codings[i].getH() for i in range(3))
        
        # encode
        self.apply_unitary(codings)
        
        # rotate
        for t in list(self.triangles.values()) :
            t.rotate()

        # apply W and decode
        self.apply_unitary([decodings[i] @ coin for i in range(3)])

        if make_pachner_moves :
            # get the 3-cycles to put down
            three_cycles_filtered = [c for c in self.three_cycles
                                     if all(c[i] in self.triangles
                                            for i in range(3))]
            three_cycles_sorted = sorted([c for c in three_cycles_filtered if self.three_cycle_probability(c) < beta], key = self.three_cycle_probability)

            # re-encode for the translations
            self.apply_unitary(codings)

            # 3-to-1 moves
            for c in three_cycles_sorted :
                self.three_to_one(c)

            # decode for the last time
            self.apply_unitary(decodings)

            # self.check()

            # 1-to-3 moves
            for t in list(self.triangles.values()) :
                p = t.probability()
                if p >= alpha :
                    self.one_to_three(t)
                elif p == 0 and t.is_basic() : # if the triangle is useless, remove it
                    self.remove_triangle(t)
                        
            # self.check()
                        

    # --- Drawings ---
    
    def draw_edges(self, ax, cmap) :
        """Draw the edges of the lattice in a subplot.
        Very inefficient.

        :param ax: The subplot's axes in which to draw.
        :param cmap: The color map to represent high and low probabilities."""
        
        for coords, edge in self.edges.items() :
            # Change of basis
            P1, P2 = self.cartesian(coords)
            ax.plot(P1, P2, color = cmap(self.edge_probability(edge)))

            
    def draw(self, ax, cmap, vmax = None) :
        """Draw a heatmap of the lattice in a subplot.

        :param ax: The subplot's axes in which to draw.
        :param cmap: The color map to represent high and low probabilities.
        :param vmax: The float that gets mapped to the highest color. 
                     If None, inferred from the data."""
    
        if len(self.triangles) >= 3 :
            probability, points = np.array([[t.probability(), t.center] for t in self.triangles.values()]).T
        else :
            probability, points = np.array([[self.edge_probability(e), e.middle] for e in self.edges.values()]).T
        X, Y = self.cartesian(points)
        triang = tri.Triangulation(X, Y)

        return ax.tricontourf(X, Y, np.real(probability), cmap = plt.cm.coolwarm, vmax = vmax)
