Usage example :

```
# load the library
from triangularLattice import *

# compute the matrices involved in the problem
W = ...
U_0 = ...
U_1 = ...
U_2 = ...
Us = (U_0, U_1, U_2)

# create a lattice object
L = Lattice()

# compute 10 steps
for t in range(10) :
	L.step(W, Us, alpha, beta)
	
# get the variances
varx, vary = L.variance()

# get the curvature in a ball
curvature = 0
for v in L.vertices.keys() :
	if L.is_in_ball(0, 1, v) :
		curvature += L.curvature(v)
		
import matplotlib as pyplot
fig, axes = plt.subplots(ncols = 2)
# draw a heatmap 
result = L.draw(axes[0], plt.cm.coolwarm)
# draw every edge (much less efficient)
L.draw_edges(axes[1], plt.cm.coolwarm)
```
